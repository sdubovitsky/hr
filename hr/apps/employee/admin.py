from django.contrib import admin

from .models import Employee


class EmployeeAdmin(admin.ModelAdmin):
    list_display = ('id', 'last_name', 'first_name', 'patronymic', 'department')
    search_fields = ('last_name', 'first_name')
    list_filter = ('department',)


admin.site.register(Employee, EmployeeAdmin)
