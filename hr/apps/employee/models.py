from django.db import models

from department.models import Department


class Employee(models.Model):
    department = models.ForeignKey(
        Department,
        verbose_name='Департамент',
        on_delete=models.CASCADE,
    )
    last_name = models.CharField(
        'Фамилия',
        max_length=255,
    )
    first_name = models.CharField(
        'Имя',
        max_length=255,
    )
    patronymic = models.CharField(
        'Отчество',
        max_length=255,
        blank=True,
    )

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.patronymic}'
