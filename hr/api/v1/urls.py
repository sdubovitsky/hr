from rest_framework.routers import DefaultRouter
from .departments import views as departments_views
from .employees import views as employees_views


router = DefaultRouter()

router.register('departments',
                departments_views.DepartmentViewSet,
                basename='departments')
router.register('employees',
                employees_views.EmployeeViewSet,
                basename='employees')

urlpatterns = router.urls
