from django.urls import reverse
from rest_framework.test import APITestCase


class DepartmentsTest(APITestCase):

    def test_smoke(self):
        list_url = reverse('api:v1:departments-list')
        name = 'department 1'
        resp = self.client.post(list_url, {'name': name})
        self.assertEqual(resp.status_code, 201)
        department_id = resp.data['id']

        resp = self.client.get(list_url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data), 1)

        detail_url = reverse('api:v1:departments-detail',
                             kwargs={'pk': department_id})
        resp = self.client.get(detail_url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data['name'], name)

        new_name = 'new_name'
        resp = self.client.patch(detail_url, {'name': new_name})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data['name'], new_name)

        resp = self.client.delete(detail_url)
        self.assertEqual(resp.status_code, 204)
