from django.urls import reverse
from rest_framework.test import APITestCase

from department.models import Department


class EmployeesTest(APITestCase):

    def test_smoke(self):
        list_url = reverse('api:v1:employees-list')
        employee_data = {
            'first_name': 'Иван',
            'last_name': 'Иванов'
        }
        resp = self.client.post(list_url, employee_data)
        self.assertEqual(resp.status_code, 400)

        department = Department.objects.create(name='dep 1')

        employee_data['department'] = department.id
        resp = self.client.post(list_url, employee_data)
        self.assertEqual(resp.status_code, 201)

        employee_id = resp.data['id']

        resp = self.client.get(list_url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(len(resp.data), 1)

        detail_url = reverse('api:v1:employees-detail',
                             kwargs={'pk': employee_id})
        resp = self.client.get(detail_url)
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data['last_name'], employee_data['last_name'])

        new_last_name = 'new_last_name'
        resp = self.client.patch(detail_url, {'last_name': new_last_name})
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(resp.data['last_name'], new_last_name)

        resp = self.client.delete(detail_url)
        self.assertEqual(resp.status_code, 204)
